# Bakkiecounter

Welcome to the Bakkiecounter documentation. The BakkieCounter is a small web application that supports you in tracking your coffee consumption. It is as simple as that. No fancy features, no elaborated user interface. Just a simple web application. The code is open-source, the use is free of charge.

This site provides documentation to host a Bakkiecounter instance yourself. Or, you can use the official instance [here](https://bakkiecounter.com).

Feel free to browse around. This site provides documentation for both the [backend](/docs/backend) and [frontend](/docs/frontend), which can also be found in the sidebar.

The source code can be found on [Codeberg](https://codeberg.org/bakkie2go).
