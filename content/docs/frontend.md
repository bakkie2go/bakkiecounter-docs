---
title: Frontend
---

## Prerequisites

* [`npm`](https://www.npmjs.com/)
* [`git`](https://git-scm.com/), or a way to download the repository manually

## Getting started

Generating the frontend is fairly trivial. Firstly, clone or download the repository:

```bash
git clone https://codeberg.org/bakkie2go/bakkiecounter-frontend.git
```

Then, install all the dependencies:

```bash
npm install
```

Then, build the frontend:

```bash
npm run build
```

Then, use a webserver like [`nginx`](https://nginx.org/en/) to host the generated frontend.
