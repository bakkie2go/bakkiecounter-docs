---
title: Backend
---

## Prerequisites

* Python `^3.8.5`
* A [`tortoise-orm`](https://tortoise-orm.readthedocs.io/)-compatible DBMS (Note: we have only used PostgreSQL, other DB systems may not work).
* [`poetry`](https://python-poetry.org/), a Python dependency manager
* [`git`](https://git-scm.com/), or a way to download the repository manually

## Getting started

Firstly, clone or download the repository:

```bash
git clone https://codeberg.org/bakkie2go/bakkiecounter-backend.git
```

Then, create the database:

```bash
createdb bakkiecounter
```

Next, install the Python dependencies.

```bash
poetry install --no-dev
```

From now on, we assume that you are inside a `poetry` shell:

```bash
poetry shell
```

Now, you run the backend for the first time:

```bash
python -m bakkiecounter
[2022-07-05 16:43:49 +0200] [20971] [INFO] Initialized configuration file config.toml.
[2022-07-05 16:43:49 +0200] [20971] [INFO] Please configure and restart the server.
[2022-07-05 16:43:49 +0200] [20971] [INFO] Shutdown server
```

This dumps a config file (`config.toml`), which must be edited. The configuration file is in [TOML format](https://toml.io/en/).

### Editing the config file

There are a couple fields that must be edited. We provide some explanation:

```toml
# config.toml
[general]
dev = false # Whether the server should show logs. Leave to false for production mode
secret_jwt_token = ">;6)%KD@2TvdP*u3<mo-bsx0~`c.$REZnI/5" # DO NOT SHARE! This is a JWT secret. See: https://jwt.io
domain = "foo.com" # Domain name of the service, required to properly send cookies

[database] # Trivial database info
host = "localhost"
port = 5432
name = "name" # Name of the db
user = "postgres"
password = "secret"

[log] # Locations of the log files. Make sure to create folders and fix permissions to own user!
access = "/var/log/bakkiecounter/access.log"
error = "/var/log/bakkiecounter/error.log"

[web] # Host and port of the backend
host = "0.0.0.0"
port = 4885
```

### Starting the server the first time

If you start the server the first time, you can start the server with the `--migrate` flag in order to instruct the server to initialize the database. Make sure, the configured database user has granted permissions on creating tables.

```bash
python -m bakkiecounter --migrate
```

## systemd/OpenRC unit

To start the backend on system startup, this repository provides a systemd unit located in `bakkiecounter.service`. It also contains an OpenRC init script.

PRs for other init systems are welcome!

## Automatic database backups

For this, we would like to link to the [PostgreSQL wiki 😀](https://wiki.postgresql.org/wiki/Automated_Backup_on_Linux)
